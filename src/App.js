import Navbar from './components/Navbar'
import Home from './components/Home';

import { //Page routing
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'

//Main app navigation and content loading
function App() {
  
  return (
    <Router>
    <div className="App">
      <Navbar />
      <div className="content">
        <Switch>

          <Route exact path="/">
            <Home />
          </Route>

        </Switch>
      </div>
      
    </div>
    </Router>
  );
}

export default App;