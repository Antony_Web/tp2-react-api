// IMPORTS
import { useEffect, useState } from "react";
import ReactPaginate from "react-paginate"; //npm package

const Content = () => { //main component

    const handlePageClick = async (data) => { //handle the pagination click when changing page
        console.log("Changing page...", data.selected)
        let currentPage = data.selected + 1

        const catsServer = await fetchCats(currentPage)

        setCats(catsServer)
    }

    const [cats, setCats] = useState([]) //main hook

    useEffect(() => {
        const getCats = async () => { // asynchronous fetch
            const res = await fetch(
                "https://api.thecatapi.com/v1/images/search?limit=9&page=10",
                {
                headers: { //liscencing
                    "Content-Type": "application/json",
                    "x-api-key": "8e926a14-e5e6-47d3-b560-70467d14d273"
                }})
                const data = await res.json()
                setCats(data)
        }

        getCats()
    }, [])

    console.log(cats) //log the data fetched

    const fetchCats = async (currentPage) => { //fetch cats for the current page
        const res = await fetch(
            `https://api.thecatapi.com/v1/images/search?limit=9&page=${currentPage}`
        )
        const data = await res.json()
        return data
    }


    return ( // rendering the fetched data in HTML form
        
        <div className="catBox">
        {cats.map((cat) => {
            return <img key={cat.id} src={cat.url} className="catImg catBox" />
        })}

        <ReactPaginate 
        previousLabel={'<'}
        nextLabel={'>'}
        breakLabel={'...'}
        pageCount={10}
        marginPagesDisplayed={2}
        pageRangeDisplayed={3}
        onPageChange={handlePageClick}
        containerClassName={'pagination justify-content-center mt-5'}
        pageClassName={'page-item'}
        pageLinkClassName={'page-link'}
        previousClassName={'page-item'}
        previousLinkClassName={'page-link'}
        nextClassName={'page-item'}
        nextLinkClassName={'page-link'}
        breakClassName={'page-item'}
        breakLinkClassName={'page-link'}
        activeClassName={'active'}
        
        />
        </div>
        
     );
}
 
export default Content;