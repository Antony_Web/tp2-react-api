import logo from '../logo.svg'
import '../App.css'
import { //Page Routing
    BrowserRouter as Router,
    Route,
    Switch
  } from 'react-router-dom'

const Navbar = () => {
    return ( 
        <Router>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="container-fluid">
                <img src={logo} className="App-logo" alt="logo" />
                <span className="navbar-brand" href="#">Cat API</span>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <a className="nav-link" aria-current="page" href="/">Home</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        </Router>
    );
}
 
export default Navbar;