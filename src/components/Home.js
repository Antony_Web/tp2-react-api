//IMPORTS
import Content from "./Content";

const Home = () => {

    return ( 

        <div className="Home container mt-5">
            <h2 className="text-center">Welcome to the Cat API !</h2>
            <h6 className="text-center"><i>Enjoy the kittens!</i></h6>
            <main>
                <Content />
            </main>
        </div>
     )
}
 
export default Home;